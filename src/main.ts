import { createApp } from "vue";
import App from "./App.vue";
import "element-plus/dist/index.css";
import axios from "axios";

const app = createApp(App);
app.provide("$axios", axios);

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/service-worker.js")
    .then(function (registration) {
      console.log("Service Worker registered with scope:", registration.scope);
    })
    .catch(function (err) {
      console.log("Service Worker registration failed:", err);
    });
}

// window.testMobile = /Mobi/i.test(navigator.userAgent);
(window as any).testMobile = /Mobi/i.test(navigator.userAgent);
app.mount("#app");
