export const keys = {
  key1: "92cc329cc7af3ecaa0dd9e4f45d1aa4f",
  key2: "2f0530c73dc78130f1345a93f76432f9",
  key3: "19940f43b20dc2cc0fba93d0e88d1a1b",
  key4: "6c30401a6aaddd51064641793cd6dcf4",
};

export const configUrl = {
  dataServerBaseUrl: process.env.VUE_APP_DATA_SERVER_URL,
};
