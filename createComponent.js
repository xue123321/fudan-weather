const fs = require("fs");
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

readline.question("Enter component name: ", (name) => {
  const componentName = name.charAt(0).toUpperCase() + name.slice(1); // Capitalize first letter
  const fileName = `${componentName}.vue`; 
  const componentCode = `<template>
  <div>
    <h1>{{ title }}</h1>
    <button @click="incrementCount">Clicked {{ count }} times</button>
  </div>
</template>

<script setup>
import { ref } from 'vue';

const title = 'Hello ${componentName}';
const count = ref(0);

function incrementCount() {
  count.value++;
}
</script>

<style scoped>
div {
  text-align: center;
}
h1 {
  color: #42b983;
}
button {
  margin-top: 20px;
}
</style>
`;

  fs.writeFile(`./src/components/${fileName}`, componentCode, (err) => {
    if (err) {
      console.error("Error creating component:", err);
      readline.close();
      return;
    }

    console.log(`${fileName} created successfully.`);
    readline.close();
  });
});
