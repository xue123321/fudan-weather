const { defineConfig } = require("@vue/cli-service");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    plugins: [
      // Add the BundleAnalyzerPlugin
      new BundleAnalyzerPlugin(),
    ],
  },
});
