self.addEventListener("fetch", (event) => {
  // Check if the request is for a .tiff file
  if (event.request.url.endsWith(".tiff")) {
    event.respondWith(
      caches.match(event.request).then((response) => {
        // Return the cached response if found
        if (response) {
          return response;
        }
        // Otherwise, fetch the file from the network
        return fetch(event.request).then((response) => {
          // Check if we received a valid response
          if (!response || response.status !== 200) {
            return response;
          }

          // Clone the response to use it in the cache and return it for the fetch
          var responseToCache = response.clone();

          caches.open("v1").then((cache) => {
            console.log("v1cache");
            cache.put(event.request, responseToCache);
          });

          return response;
        });
      })
    );
  } else {
    // For non-.tiff requests, just fetch them from the network as usual
    event.respondWith(fetch(event.request));
  }
});
